package com.kevinm.csc435;

import static spark.Spark.halt;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class GetInventory {
	
	private Gson gson = new GsonBuilder().setPrettyPrinting().create();
	private ArrayList<String> myList;
	private String jsonOutput;
	private JsonObject jsonObj;
	private JsonArray jsonArrayString;
	private String[]months = {	"january","february","march", "april"};
	
	public GetInventory(){
		super();
	}
	
	public String get(){
		
		jsonObj = new JsonObject();
				
		for(int i = 0; i < months.length; i++) {
    		
    		myList = getFromDB(months[i]);
    		jsonArrayString = gson.toJsonTree(myList).getAsJsonArray();
        	jsonObj.add(months[i], jsonArrayString);
        	
    		
    	}//END FOR
		
        jsonOutput = jsonObj.toString();

		
	
		return jsonOutput;
		
	}
	
	public String get(String month){
		
		jsonObj = new JsonObject();
		
		myList = getFromDB(month);
    	jsonArrayString = gson.toJsonTree(myList).getAsJsonArray();
    	jsonObj.add(month, jsonArrayString);
    	
        jsonOutput = jsonObj.toString();

    	
		return jsonOutput;
		
	}
	
	public String get(String month, String id){
		
		jsonObj = new JsonObject();
		
		myList = getFromDB(month, id);
    	jsonArrayString = gson.toJsonTree(myList).getAsJsonArray();
    	jsonObj.add(month, jsonArrayString);
        jsonOutput = jsonObj.toString();
        
      

		
	
		return jsonOutput;
		
	}
	
	public ArrayList<String> getFromDB(String month){
		
		myList = new ArrayList<String>();
		Connection con = null;
		Statement statement = null;
		ResultSet rs;
		String query = "SELECT * FROM ";
		
		
		try {
	        
			//Connect to the database
	        con = new DatabaseConnection().initializeDatabase();
	        
	        // Sends queries to the DB for results
	        statement = con.createStatement();
	       
	        query += month + ";";
	        
	        rs = statement.executeQuery(query);
	        
	        //iterate through the java result set
	        while (rs.next())
	        {
	          int idNum = rs.getInt("id");
	          String name = rs.getString("name");
	          String unit = rs.getString("unit");
	          double unitCost = rs.getDouble("unitCost");
	          double amountOnHand = rs.getDouble("amountOnHand");
	          double totalCost = rs.getDouble("totalCost");
	          
	          Product myProduct = new Product(idNum,name,unit,unitCost,amountOnHand, totalCost);
	          myList.add(gson.toJson(myProduct));
	     
	        }

	        // Close DB connection
	        con.close();
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
			halt("Please try again.");
			
		} 
		catch (SQLException e) {
			e.printStackTrace();
			halt("Problem reading from Database.");
			
		}
		finally {
	        try {
	            if (statement != null) statement.close();
	            if (con != null) con.close();
	        } 
	        catch (SQLException ex) {
	            ex.printStackTrace();
	        }
			
		}
		
		return myList;
	}
	
	public ArrayList<String> getFromDB(String month, String id){
		
		//local constants
		
		//local variables
		ArrayList<String> myList = new ArrayList<String>();
		Connection con = null;
		Statement statement = null;
		ResultSet rs;
		String query = "SELECT * FROM ";
		
		/******************************************/
		
		try {
	        
			//Connect to the database
	        con = new DatabaseConnection().initializeDatabase();
	        
	        // Sends queries to the DB for results
	        statement = con.createStatement();
	       
	        query += month + " where id = " + id + ";";
	        
	        rs = statement.executeQuery(query);
	        
	        //iterate through the java result set
	        while (rs.next())
	        {
	          int idNum = rs.getInt("id");
	          String name = rs.getString("name");
	          String unit = rs.getString("unit");
	          double unitCost = rs.getDouble("unitCost");
	          double amountOnHand = rs.getDouble("amountOnHand");
	          double totalCost = rs.getDouble("totalCost");
	          
	          Product myProduct = new Product(idNum,name,unit,unitCost,amountOnHand, totalCost);
	          myList.add(gson.toJson(myProduct));
	     
	        }//END WHILE
	        
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
			halt("Please try again.");
			
		} 
		catch (SQLException e) {
			e.printStackTrace();
			halt("Problem reading from Database.");
			
		}
		finally {
	        try {
	            if (statement != null) statement.close();
	            if (con != null) con.close();
	        } 
	        catch (SQLException ex) {
	            ex.printStackTrace();
	        }
			
		}
		return myList;
		
		
		
	}
	
	
	

}
