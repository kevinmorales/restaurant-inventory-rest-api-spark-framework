package com.kevinm.csc435;

import java.text.DecimalFormat;

public class Product{
	
	//class constants
	
	//class variables
	private int id;
	private String item;
	private String unit;
	private double amount;
	private double unitCost;
	private double totalCost;
	
	
	/********************/
	
	public Product() {
		this.setItem("");
		this.setUnit("");
		this.setAmount(0);
		this.setUnitCost(0);
		this.setTotalCost(0.0);
	}
	
	public Product(String name, String unit, double amount, double unitCost) {
		this.setItem(name);
		this.setUnit(unit);
		this.setAmount(amount);
		this.setUnitCost(unitCost);
		setTotalCost(amount * unitCost);
		
	}

	
	public Product(int id, String name, String unit, double amount, double unitCost, double totalCost) {
		this.setItem(name);
		this.setUnit(unit);
		this.setAmount(amount);
		this.setUnitCost(unitCost);
		this.setTotalCost(totalCost);
		this.setId(id);
		
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public double getAmount() {
		return Math.round((amount * 100.0) / 100.0);
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getUnitCost() {
		return Math.round((unitCost * 100.0) / 100.0);
	}

	public void setUnitCost(double unitCost) {
		this.unitCost = unitCost;
	}

	public double getTotalCost() {
		return Math.round((totalCost * 100.0) / 100.0);
	}

	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String toString() {
		DecimalFormat df = new DecimalFormat("#.##");  

		return id + "," + item + "," + unit + "," 
		+ df.format(unitCost) + "," + df.format(amount)
		+ "," + df.format(totalCost);
		
	}
	
}
	
	
	