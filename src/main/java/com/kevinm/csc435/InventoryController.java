package com.kevinm.csc435;

import static spark.Spark.*;
import java.io.IOException;

public class InventoryController {
	//class variables
	private GetInventory myGetter = new GetInventory();
	private AddInventory myAdder = new AddInventory();
	private DeleteInventory myDeleter = new DeleteInventory();
	private EditInventory myEditor = new EditInventory();
	private String output;
	private String input;
		
	public InventoryController() {
		super();
	}
	protected void doGet() throws IOException {
		
		//local constants
		
		//local variables
		boolean success = true;
		
		
		/**********************/
		
		get("/Manage", (req, res) -> {
			res.type("application/json; charset=UTF-8");
			
			output = myGetter.get();
			
			if(success)
				return output;
			
			else
				return "{ \"success\": false}";
	        
		});//END GET
		
		get("/Manage/:month", (req, res) -> {
			res.type("application/json; charset=UTF-8");
			
			output = myGetter.get(req.params(":month"));
			
			

			if(success)
				return output;
			
			else
				return "{ \"success\": falso}";
	        
		});//END GET
						
		get("/Manage/:month/:id", (req, res) -> {
			res.type("application/json; charset=UTF-8");
			
			output = myGetter.get(req.params(":month"),req.params(":id"));

			if(success)
				return output;
			
			else
				return "{ \"success\": false}";
	        
		});//END GET
		
	}//END doGet
	
	
	
	protected void doPost() throws IOException {
		
		//local constants
		
		//local variables

		/********************************/
		
		post("Manage/:month" , (req,res) ->{
			
			input = req.body();
									
			String output = myAdder.add(req.params(":month"), input);
			
			//RETURN ID
			return output;
			
		});
		
	}//END doPost
	
	
	protected void doPut() throws IOException {
		
		/********************************/
		
		put("Manage/:month/:id" , (req,res) ->{
			
			res.type("application/json; charset=UTF-8");
			
			System.out.println("PUT");
			
			output = myEditor.edit(req.params(":month"),req.params(":id"),req.body());

			if(!output.equals("-1"))
				return output;
			
			else
				return "{ \"success\": false}";
			
		});
        
		
	}//END doPut
	
	protected void doDelete() throws IOException {
		//local constants
		
		//local variables
				
		/**********************/
		
		delete("Manage/:month/:id" , (req,res) ->{
			
			res.type("application/json; charset=UTF-8");
			System.out.println("DELETE");
			System.out.println(req.params(":month"));
			System.out.println(req.params(":id"));
			
			output = myDeleter.delete(req.params(":month"),req.params(":id"));

			if(!output.equals("-1"))
				return output;
			
			else
				return "{ \"success\": false}";
			
		});
				
		
	}//END doDelete

}//END ManageInventoryServlet

