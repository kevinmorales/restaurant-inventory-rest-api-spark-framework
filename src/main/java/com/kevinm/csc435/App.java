package com.kevinm.csc435;

import java.io.IOException;

public class App {
	
	public static final InventoryController myController = new InventoryController();

    public static void main(String[] args) throws IOException {
    	
    	//local constants
    	
    	//local variables
    	
    	/**********************/
    	
       	runApp();
    }

	private static void runApp() throws IOException {
		
		//local constants
    	
    	//local variables
    	
    	/**********************/
		myController.doGet();
		myController.doPost();
		myController.doDelete();
		myController.doPut();
		
	}
}
