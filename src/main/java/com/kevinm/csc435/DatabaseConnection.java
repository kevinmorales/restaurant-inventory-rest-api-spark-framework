package com.kevinm.csc435;

import java.sql.Connection; 
import java.sql.DriverManager; 
import java.sql.SQLException; 


  
// This class can be used to initialize the database connection 
public class DatabaseConnection {
	
	public DatabaseConnection() {
		super();
	}
	
    public Connection initializeDatabase() throws SQLException, ClassNotFoundException 
    { 
    	//local constants
    	
    	//local variables
    	Connection con;
    	
    	/************************/
        
        // Everything needed to connect to the DB
		Class.forName("com.mysql.cj.jdbc.Driver");
		String url = "jdbc:mysql://localhost/inventory";
		String user = "kevin";
		String pw = "password";
		
		// Used to issue queries to the DB
		con = DriverManager.getConnection(url, user, pw);
        return con; 
    } 
}
    
    