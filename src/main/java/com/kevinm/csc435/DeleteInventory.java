package com.kevinm.csc435;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.google.gson.Gson;

public class DeleteInventory {
	
	private Gson gson = new Gson();
	
	public DeleteInventory(){
		super();
	}
	
	public String delete(String month, String id) {
		
		//local constants
		
		//local variables
		String jsonOutput;
		/*****************/
		
		jsonOutput = deleteFromDb(month, id);
		
		return jsonOutput;
		
	}
	
	private String deleteFromDb(String month, String id) {
		
		//local constants
		
		//local variables
		Connection con;
		ResultSet rs;
		Statement statement;
		String query;
		String output = "-1";
		Product myProduct = new Product();
		
		/********************/
		
		try {
			
	        con = new DatabaseConnection().initializeDatabase();
	        
	        statement = con.createStatement();
	        
	        query = "SELECT * FROM " + month + " WHERE id = " + id + ";";
	        
	        rs = statement.executeQuery(query);
	        
	        if (!rs.isBeforeFirst() ) {    
	            	        	
	        	// Close DB connection
		        con.close();
	        } 
	        else {
	        	
	        	while (rs.next())
		        {
		          int idNum = rs.getInt("id");
		          String name = rs.getString("name");
		          String unit = rs.getString("unit");
		          double unitCost = rs.getDouble("unitCost");
		          double amountOnHand = rs.getDouble("amountOnHand");
		          double totalCost = rs.getDouble("totalCost");
		          
		          myProduct = new Product(idNum,name,unit,unitCost,amountOnHand, totalCost);
		          
		        }
	        	
	        	output = gson.toJson(myProduct);
	        	
	        	statement.close();
	        
		        statement = con.createStatement();
		
		        // Add a new entry
		        query = 
		        "DELETE FROM " 	+ month	+  " WHERE id = " 	+ id 	+ ";" ;
		        	       
		        // Execute the Query
		        statement.executeUpdate(query);
		        
		        // Close DB connection
		        con.close();
		        statement.close();
		        
	        } 
	        
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return output;
				
	}//END editDb
}
