package com.kevinm.csc435;

import java.sql.*;
import java.util.HashMap;

public class EditInventory {
	
	public EditInventory(){
		super();
	}
	
	public String edit(String month, String id, String jsonInput) {
		String output = "-1";
		boolean success = false;
		HashMap<String,String> jsonMap;
		
		System.out.println(jsonInput);
		
		jsonMap = parseJson(jsonInput);
		
		if(jsonMap.containsKey("unit")) {
			
			success = editDb(month, id, "unit", jsonMap.get("unit"));
			
			if (success) 
				output = "{\"success\": true}";
			
		}
		
		if(jsonMap.containsKey("unitCost")) {
			
			success = editDb(month, id, "unitCost", Double.valueOf(jsonMap.get("unitCost")));
			
			if (success) 
				output = "{\"success\": true}";
			
		}
		
		if(jsonMap.containsKey("item")) {
			
			success = editDb(month, id, "name", jsonMap.get("item"));
			
			if (success) 
				output = "{\"success\": true}";
			
		}
		
		if(jsonMap.containsKey("amount")) {
			
			success = editDb(month, id, "amountOnHand", Double.valueOf(jsonMap.get("amount")));
			
			if (success) 
				output = "{\"success\": true}";
			
		}
		
		updateTotalCost(month, id);
					
		return output;
		
	}
	
	private boolean editDb(String month, String id, String attribute, String value) {
		
		//local constants
		
		//local variables
		Connection con = null;
		Statement statement = null;
		boolean success = false;
		String query;
		ResultSet rs;

		
		/********************/
		
		try {
			
	        con = new DatabaseConnection().initializeDatabase();
	        
	        statement = con.createStatement();
	        
	        query = "SELECT * FROM " + month + " WHERE id = " + id + ";";
	        
	        rs = statement.executeQuery(query);
	        
	        if (!rs.isBeforeFirst() ) {    
	            	        	
	        	// Close DB connection
		        con.close();
		        statement.close();
		        success = false;
	        } 
	        else {
	        
		        // Sends queries to the DB for results
		        statement = con.createStatement();
		        
		        // Add a new entry
		        query = "UPDATE " + month + 
		        		" SET " + attribute + " = '" + value + "' WHERE id = " + id + ";";
		        System.out.println(query);
		        
		        // Execute the Query
		        statement.executeUpdate(query);
		        
		        // Close DB connection
		        con.close();
		        
		        success = true;
		        
	        }
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return success;
		
	}
	
	private boolean editDb(String month, String id, String attribute, Double value) {
		
		//local constants
		
		//local variables
		Connection con = null;
		Statement statement = null;
		boolean success = false;
		String query;
		ResultSet rs;

		
		/********************/
		
		try {
			
	        con = new DatabaseConnection().initializeDatabase();
	        
	        statement = con.createStatement();
	        
	        query = "SELECT * FROM " + month + " WHERE id = " + id + ";";
	        
	        rs = statement.executeQuery(query);
	        
	        if (!rs.isBeforeFirst() ) {    
	            	        	
	        	// Close DB connection
		        con.close();
		        statement.close();
		        success = false;
	        } 
	        else {
	        
		        // Sends queries to the DB for results
		        statement = con.createStatement();
		        
		        // Add a new entry
		        query = "UPDATE " + month + 
		        		" SET " + attribute + " = " + value + " WHERE id = " + id + ";";
		        
		        System.out.println(query);
		        // Execute the Query
		        statement.executeUpdate(query);
		        
		        // Close DB connection
		        con.close();
		        
		        success = true;
		        
	        }
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return success;
		
	}
	
	private void updateTotalCost(String month, String id) {
		
		//local constants
		
		//local variables
		Connection con = null;
		Statement statement = null;
		String query;
		ResultSet rs;
		double amountOnHand = 0;
		double unitCost = 0;
		double totalCost;

		/********************/
		
		try {
		
			con = new DatabaseConnection().initializeDatabase();
	        
	        statement = con.createStatement();
	        
	        query = "SELECT amountOnHand, unitCost FROM " + month + " WHERE id = " + id + ";";
	        
	        rs = statement.executeQuery(query);
	        
	        if (!rs.isBeforeFirst() ) {    
	            	        	
	        	// Close DB connection
		        con.close();
		        statement.close();
	        } 
	        else {
	        	
	        	while (rs.next())
		        {
		          unitCost = rs.getDouble("unitCost");
		          amountOnHand = rs.getDouble("amountOnHand");
		          		          
		        }
	        	
	        	totalCost = unitCost * amountOnHand;
	        	
		        // Sends queries to the DB for results
		        statement = con.createStatement();
		        
		        // Add a new entry
		        query = "UPDATE " + month + 
		        		" SET totalCost = " + totalCost + " WHERE id = " + id + ";";
		        
		        System.out.println(query);
		        // Execute the Query
		        statement.executeUpdate(query);
		        
		        // Close DB connection
		        con.close();
		        statement.close();
	        }
	        
        } 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
				
		
		
	}
	
	private HashMap<String,String> parseJson(String json){
		
		//local constants
		
		//local variables
		HashMap<String,String> myMap = new HashMap<String,String>();
		String[] jsonSplitArray;
		
		/************************/
		
		//Parse json into comma separated values
		json = json.replace("}", "").replace("\"", "").replace("{", "");
		
		//Split on the commas
		jsonSplitArray = json.split(",");
		
		
		for(int i = 0;i<jsonSplitArray.length;i++)
			
			myMap.put(jsonSplitArray[i].split(":")[0],jsonSplitArray[i].split(":")[1]);
		
		//END FOR
		
		return myMap;
		
	}
	
}
	
	
