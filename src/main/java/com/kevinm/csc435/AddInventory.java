package com.kevinm.csc435;
import static spark.Spark.*;
import java.sql.*;
import com.google.gson.*;


public class AddInventory {
	
	
	private Gson gson = new Gson();
	
	public AddInventory() {
		super();
	}
	
	public String add(String month, String inputJson){
		
		int idNum;
		String output;
		
		Product myProduct = gson.fromJson(inputJson,Product.class);
		
		myProduct.setTotalCost(myProduct.getAmount() * myProduct.getUnitCost());
		
		idNum = AddToDb(myProduct, month);
		
		if(idNum != -1) 
			
			output = "{\"id\": " + idNum + "}";
		
		else
			
			output = "{\"success\": false}";
					
		//END IF
		
		return output;
		
	}
	
public int AddToDb(Product x, String month) {
		
		//local constants
		
		//local variables
		Connection con = null;
		ResultSet rs;
		Statement statement = null;
		String query;
		int idNum = -1;
		
		/**********************/
		
		try {
	        
	        // Used to issue queries to the DB
			
	        con = new DatabaseConnection().initializeDatabase();
	        
	        // Sends queries to the DB for results
	        statement = con.createStatement();
	        
	        // Add a new entry
	        query = "INSERT INTO " + month + " " +
	        "(id, name, unit, unitCost, amountOnHand, totalCost) " + 
	        "VALUES (NULL" + ", '"
	        + x.getItem() + "', '"
	        + x.getUnit() + "'," 
	        + x.getUnitCost() + ", " 
	        + x.getAmount() + ", "
	        + x.getTotalCost() + ")";
	        
	        
	        // Execute the Query
	        statement.executeUpdate(query);
	        
	        statement.close();
	        
	        statement = con.createStatement();
	        
	        query = "SELECT id FROM " + month  + " WHERE name = " + "'" + x.getItem() + "'";
	        rs = statement.executeQuery(query);
	        
	        while (rs.next())
	        {
	        	idNum = rs.getInt("id");
	        }
	        // Close DB connection
	        con.close();
	        
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
			halt("Please try again.");
			
		} 
		catch (SQLException e) {
			e.printStackTrace();
			halt("Problem reading from Database.");
			
		}
		finally {
	        try {
	            if (statement != null) statement.close();
	            if (con != null) con.close();
	        } 
	        catch (SQLException ex) {
	            ex.printStackTrace();
	        }
			
		}
		return idNum;
		
		
	}

}
